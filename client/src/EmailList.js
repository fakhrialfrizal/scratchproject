import React from "react";
// import Link from "@material-ui/core/Link";
// import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Select from "@material-ui/core/Select";
import Title from "./Title";
import Axios from "axios";
import { MenuItem, TableContainer } from "@material-ui/core";
// import { DataGrid } from "@material-ui/data-grid";

// Generate subdomain Data
function createSubdomain(item) {
  const obj = {
    id: item.id,
    sopd: item.sopd,
    alamatInstansi: item.alamatInstansi,
    teleponInstansi: item.teleponInstansi,
    emailInstansi: item.emailInstansi,
    namaAdministratif: item.namaAdministratif,
    nipAdministratif: item.nipAdministratif,
    jabatanAdministratif: item.jabatanAdministratif,
    emailAdministratif: item.emailAdministratif,
    teleponAdministratif: item.teleponAdministratif,
    namaTeknis: item.namaTeknis,
    instansiTeknis: item.instansiTeknis,
    alamatTeknis: item.alamatTeknis,
    emailTeknis: item.emailTeknis,
    teleponTeknis: item.teleponTeknis,
    namaApp: item.namaApp,
    alamatApp: item.alamatApp,
    subdomainApp: item.subdomainApp,
    detilApp: item.detilApp,
    kebutuhanHwApp: item.kebutuhanHwApp,
    kebutuhanSwApp: item.kebutuhanSwApp,
    nomorRekomendasi: item.nomorRekomendasi,
  };

  return obj;
}

// function createData(
//   id,
//   sopd,
//   alamatInstansi,
//   teleponInstansi,
//   emailInstansi,
//   namaAdministratif,
//   nipAdministratif,
//   jabatanAdministratif,
//   emailAdministratif,
//   teleponAdministratif,
//   namaTeknis,
//   instansiTeknis,
//   alamatTeknis,
//   emailTeknis,
//   teleponTeknis,
//   namaApp,
//   alamatApp,
//   subdomainApp,
//   detilApp,
//   kebutuhanHwApp,
//   kebutuhanSwApp,
//   nomorRekomendasi
// ) {
//   return {
//     id,
//     sopd,
//     alamatInstansi,
//     teleponInstansi,
//     emailInstansi,
//     namaAdministratif,
//     nipAdministratif,
//     jabatanAdministratif,
//     emailAdministratif,
//     teleponAdministratif,
//     namaTeknis,
//     instansiTeknis,
//     alamatTeknis,
//     emailTeknis,
//     teleponTeknis,
//     namaApp,
//     alamatApp,
//     subdomainApp,
//     detilApp,
//     kebutuhanHwApp,
//     kebutuhanSwApp,
//     nomorRekomendasi,
//   };
// }

// function preventDefault(event) {
//   event.preventDefault();
// }

// const useStyles = makeStyles((theme) => ({
//   seeMore: {
//     marginTop: theme.spacing(3),
//   },
// }));

export default function SubdomainList() {
  // const classes = useStyles();
  // const [subdomain, setSubdomain] = React.useState([]);
  const [rows, setRows] = React.useState([]);
  const [petugas, setPetugas] = React.useState([]);

  const [idPetugas, setidPetugas] = React.useState("");

  const handleidPetugas = (event) => {
    setidPetugas(event.target.value);
    // console.log("id Petugas sekarang :", event.target.value);
  };
  console.log("id petugas", idPetugas);

  React.useEffect(() => {
    Axios.get("http://localhost:3001/api/getuser").then((response) => {
      setPetugas(response.data);
    });
  }, []);

  React.useEffect(() => {
    Axios.get("http://localhost:3001/api/getsubdomain").then((response) =>
      // console.log(response.data)
      {
        // setSubdomain(response.data);
        let data = [];
        response.data.forEach((item) => {
          data.push(createSubdomain(item));
        });
        setRows(data);
      }
    );
  }, []);

  console.log("check isi ROWS", rows);
  return (
    <React.Fragment>
      {/* <Paper sx={{ width: "100%", overflow: "hidden" }}> */}
      <Title>Daftar Permohonan Email</Title>
      <TableContainer sx={{ maxHeight: 440 }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <TableCell>SOPD asal</TableCell>
              <TableCell>Alamat SOPD</TableCell>
              <TableCell>No. Telepon SOPD</TableCell>
              <TableCell>Email SOPD</TableCell>
              <TableCell align="right">
                Nama Penanggung Jawab Administratif
              </TableCell>
              <TableCell align="left">
                NIP Penanggung Jawab Administratif
              </TableCell>
              <TableCell align="left">
                Jabatan Penanggung Jawab Administratif
              </TableCell>
              <TableCell align="left">
                Email Penanggung Jawab Administratif
              </TableCell>
              <TableCell align="left">
                No. Telepon Penanggung Jawab Administratif
              </TableCell>
              <TableCell align="right">Nama Penanggung Jawab Teknis</TableCell>
              <TableCell align="left">NIP Penanggung Jawab Teknis</TableCell>
              <TableCell align="left">
                Jabatan Penanggung Jawab Teknis
              </TableCell>
              <TableCell align="left">Email Penanggung Jawab Teknis</TableCell>
              <TableCell align="left">
                No. Telepon Penanggung Jawab Teknis
              </TableCell>
              <TableCell align="right">Nama Aplikasi</TableCell>
              <TableCell>Alamat IP Aplikasi</TableCell>
              <TableCell>Subdomain Aplikasi</TableCell>
              <TableCell>Detail Aplikasi</TableCell>
              <TableCell>Kebutuhan Hardware</TableCell>
              <TableCell>Kebutuhan Software</TableCell>
              <TableCell>Nomor Rekomendasi</TableCell>
              <TableCell>Petugas</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={row.id}>
                <TableCell>{row.sopd}</TableCell>
                <TableCell>{row.alamatInstansi}</TableCell>
                <TableCell>{row.teleponInstansi}</TableCell>
                <TableCell>{row.emailInstansi}</TableCell>
                <TableCell align="left">{row.namaAdministratif}</TableCell>
                <TableCell>{row.nipAdministratif}</TableCell>
                <TableCell>{row.jabatanAdministratif}</TableCell>
                <TableCell>{row.emailAdministratif}</TableCell>
                <TableCell>{row.teleponAdministratif}</TableCell>
                <TableCell>{row.namaTeknis}</TableCell>
                <TableCell>{row.instansiTeknis}</TableCell>
                <TableCell>{row.alamatTeknis}</TableCell>
                <TableCell>{row.emailTeknis}</TableCell>
                <TableCell>{row.teleponTeknis}</TableCell>
                <TableCell>{row.namaApp}</TableCell>
                <TableCell>{row.alamatApp}</TableCell>
                <TableCell>{row.subdomainApp}</TableCell>
                <TableCell>{row.detilApp}</TableCell>
                <TableCell>{row.kebutuhanHwApp}</TableCell>
                <TableCell>{row.kebutuhanSwApp}</TableCell>
                <TableCell>{row.nomorRekomendasi}</TableCell>
                <TableCell>
                  <Select onChange={handleidPetugas} value={idPetugas}>
                    <MenuItem value="" selected></MenuItem>
                    {petugas.map((nama) => (
                      <MenuItem key={nama.id} value={nama.id}>
                        {nama.nama}
                      </MenuItem>
                    ))}
                  </Select>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      {/* <div className={classes.seeMore}>
        <Link color="primary" href="#" onClick={preventDefault}>
          See more orders
        </Link>
      </div> */}
      {/* </Paper> */}
    </React.Fragment>
  );
}
