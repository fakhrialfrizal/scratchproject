import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
// import App from "./App";
import Dashboard from "./Dashboard";

ReactDOM.render(
  <BrowserRouter>
    {/* <App></App> */}
    <Dashboard></Dashboard>
  </BrowserRouter>,
  document.getElementById("root")
);
