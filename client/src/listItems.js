import React from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import DomainIcon from "@material-ui/icons/Domain";
import DnsIcon from "@material-ui/icons/Dns";
import EmailIcon from "@material-ui/icons/Email";
import AssignmentIcon from "@material-ui/icons/Assignment";
// import { MemoryRouter as Router, useHistory } from "react-router";
import { Link as RouterLink } from "react-router-dom";

export const mainListItems = (
  <div>
    {/* <RouterLink to="/colocation">test</RouterLink> */}
    <ListItem button component={RouterLink} to="/subdomain">
      <ListItemIcon>
        <DomainIcon />
      </ListItemIcon>
      <ListItemText primary="Subdomain" />
    </ListItem>
    {/* </Link> */}
    <ListItem button component={RouterLink} to="/colocation">
      <ListItemIcon>
        <DnsIcon />
      </ListItemIcon>
      <ListItemText primary="Colocation" />
    </ListItem>
    <ListItem button component={RouterLink} to="/email">
      <ListItemIcon>
        <EmailIcon />
      </ListItemIcon>
      <ListItemText primary="Email" />
    </ListItem>
  </div>
);

export const secondaryListItems = (
  <div>
    <ListSubheader inset>Saved reports</ListSubheader>
    <ListItem button>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Current month" />
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Last quarter" />
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Year-end sale" />
    </ListItem>
  </div>
);
