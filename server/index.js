import express from "express";
import mysql from "mysql";
import bodyParser from "body-parser";
import cors from "cors";
import path from "path";
/////////////////////buat bikin _dirname/////////////////
import { fileURLToPath } from "url";
import { dirname } from "path";

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
/////////////endof buat bikin dirname/////////////////

const app = express();
const PORT = 3001;

// app.use(express.json);
app.use(cors());
app.use(express.urlencoded({ extended: true }));

const db = mysql.createPool({
  host: "localhost",
  user: "root",
  password: "",
  database: "digitize-kominfodiy",
});

const sqlBuilder = (param, table) => {
  const variables = [];

  Object.keys(param).forEach((item) => {
    variables.push(item);
  });
  console.log(variables);
  let words_var = "";
  let words_val = "";

  variables.forEach((item, index) => {
    if (variables.length === index + 1) {
      const value = param[item] !== undefined ? param[item] : "9";
      words_var += `${item}`;
      words_val += `"${value}"`;
    } else {
      const value = param[item] !== undefined ? param[item] : "9";
      words_var += `${item}, `;
      words_val += `"${value}", `;
    }
  });

  return `INSERT INTO ${table} (${words_var}) VALUES (${words_val})`;
};

//insert into database subdomain
app.post("/api/insertsubdomainhosting", (req, res) => {
  let table = "subdomainhosting";
  console.log("CHECK BODY", req.body);
  console.log("CHECK QUERY", sqlBuilder(req.body, table));
  console.log("CHECK TABLE ", table);
  try {
    db.query(sqlBuilder(req.body, table), async (err, result) => {
      console.log("CHECK RESULT");
      res.send({ status: 200, response: { err, result } });
    });
  } catch (error) {
    console.log("CHECK ERROR", error);
  }
});

//insert into database colocation
app.post("/api/insertcolocation", (req, res) => {
  let table = "colocation";
  // console.log("CHECK BODY", req.body);
  // console.log("CHECK QUERY", sqlBuilder(req.body, table));
  // console.log("CHECK TABLE ", table);
  try {
    db.query(sqlBuilder(req.body, table), async (err, result) => {
      // console.log("CHECK RESULT");
      res.send({ status: 200, response: { err, result } });
    });
  } catch (error) {
    console.log("CHECK ERROR", error);
  }
});

//insert into database email instansi
app.post("/api/insertemailinstansi", (req, res) => {
  let table = "emailinstansi";
  // console.log("CHECK BODY", req.body);
  // console.log("CHECK QUERY", sqlBuilder(req.body, table));
  // console.log("CHECK TABLE ", table);
  try {
    db.query(sqlBuilder(req.body, table), async (err, result) => {
      // console.log("CHECK RESULT");
      res.send({ status: 200, response: { err, result } });
    });
  } catch (error) {
    console.log("CHECK ERROR", error);
  }
});

//insert into database email instansi
app.post("/api/insertemailpersonil", (req, res) => {
  let table = "emailpersonil";
  // console.log("CHECK BODY", req.body);
  // console.log("CHECK QUERY", sqlBuilder(req.body, table));
  // console.log("CHECK TABLE ", table);
  try {
    db.query(sqlBuilder(req.body, table), async (err, result) => {
      // console.log("CHECK RESULT");
      res.send({ status: 200, response: { err, result } });
    });
  } catch (error) {
    console.log("CHECK ERROR", error);
  }
});

//get data from subdomain
app.get("/api/getsubdomain", (req, res) => {
  const sql = "SELECT * FROM subdomainhosting";
  console.log(sql);
  db.query(sql, (err, result) => {
    // console.log(result);
    if (result.length < 1) {
      console.log("No data in Database");
      res.send("No data in Database");
    } else {
      res.send(result);
    }
  });
});
//get data from colocation
//get data from emailinstansi
//get data from emailpersonil

//get data from user
app.get("/api/getuser", (req, res) => {
  const sql = "SELECT * FROM user";
  console.log(sql);
  db.query(sql, (err, result) => {
    if (result.length < 1) {
      console.log("No data in Database");
      res.send("No data in Database");
    } else {
      res.send(result);
    }
  });
});

//delete from subdomain
app.delete(`/api/deletesubdomain/:idRows`, (req, res) => {
  const idToDelete = req.params.idRows;
  // console.log("CHECK ID REQ", req.params.idRows);
  // console.log(req.params.idRows);
  const sql = `DELETE FROM subdomainhosting WHERE id="${idToDelete}"`;
  // console.log(sql);
  db.query(sql, (err, result) => {
    if (err) {
      console.log(err);
    }
  });
});

//call form subdomain hosting
app.get("/formsubdomain", (req, res) => {
  // res.sendFile(path.join(__dirname, "./subdomainhosting.html"));
  res.sendFile("subdomainhosting.html", {
    root: path.join(__dirname, "/form"),
  });
});

///call form colocation
app.get("/formcolocation", (req, res) => {
  // res.sendFile(path.join(__dirname, "./subdomainhosting.html"));
  res.sendFile("colocation.html", {
    root: path.join(__dirname, "/form"),
  });
});

///call form email instansi
app.get("/formemailinstansi", (req, res) => {
  // res.sendFile(path.join(__dirname, "./subdomainhosting.html"));
  res.sendFile("email instansi.html", {
    root: path.join(__dirname, "/form"),
  });
});

//call form email instansi
app.get("/formemailpersonil", (req, res) => {
  // res.sendFile(path.join(__dirname, "./subdomainhosting.html"));
  res.sendFile("email personil.html", {
    root: path.join(__dirname, "/form"),
  });
});

app.listen(PORT, () => {
  console.log(`running on port ${PORT}`);
});
